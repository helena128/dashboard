package com.helena128;

import javax.servlet.annotation.WebServlet;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.*;

/**
 * This UI is the application entry point. A UI may either represent a browser window 
 * (or tab) or some part of an HTML page where a Vaadin application is embedded.
 * <p>
 * The UI is initialized using {@link #init(VaadinRequest)}. This method is intended to be 
 * overridden to add component to the user interface and initialize non-component functionality.
 */
@Theme("mytheme")
public class MyUI extends UI {

    @Override
    protected void init(VaadinRequest vaadinRequest) {
        HorizontalLayout layout = new HorizontalLayout();
        Button addComponentButton = new Button("Add");
        addComponentButton.addStyleName("add-component-button");
        // TODO: multiple rows
        layout.addComponent(addComponentButton);
        Panel panel = new Panel("Content here");
        panel.addStyleName("component-container");
        layout.addComponent(panel);

        Panel mainPanel = new Panel();
        mainPanel.setContent(layout);
        mainPanel.addStyleName("main-panel");

        FormLayout outerLayout = new FormLayout();
        outerLayout.addComponent(mainPanel);
        // TODO: center

        setContent(outerLayout);
    }

    @WebServlet(urlPatterns = "/*", name = "MyUIServlet", asyncSupported = true)
    @VaadinServletConfiguration(ui = MyUI.class, productionMode = false)
    public static class MyUIServlet extends VaadinServlet {
    }
}
